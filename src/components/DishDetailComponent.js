import React, { Component } from "react"
import { Card, CardImg, CardTitle, CardBody, CardText } from 'reactstrap'


class DishDetails extends Component {


    renderDish(dish) {

        if (dish != null)
            return (
                <Card>
                    <CardImg top src={dish.image} alt={dish.name} />
                    <CardBody>
                        <CardTitle>{dish.name}</CardTitle>
                        <CardText>{dish.description}</CardText>
                    </CardBody>
                </Card>
            );
        else
            return (
                <div></div>
            );
    }

    renderComments(comments) {
        if (comments != null) {
            return (
                <div className="col-12 col-md-5 m-1">
                    <Card>
                        <CardBody>
                            <CardTitle>Comments</CardTitle>
                            <CardText>
                                {comments.map((comment) => {
                                    return (
                                        <ul key={comment.id} className="list-unstyled">
                                            <li>{comment.comment}</li>
                                            <li> -- {comment.author},{new Intl.DateTimeFormat('en-US',{year:'numeric',month:'short',day:'2-digit'}).format(new Date(Date.parsec(comment.date)))} </li>
                                        </ul>
                                    );
                                })
                                }
                            </CardText>
                        </CardBody>
                    </Card>

                </div>
            );
        } else {
            return (
                <div></div>
            );
        }
    }

    render() {


        return (

            <div className="row">
                <div className="col-12 col-md-5 m-1">
                    {this.renderDish(this.props.selectedDish)}
                </div>
                <div className="col-12  offset-1 col-md-5">
                    {this.renderComments(this.props.selectedDish ? this.props.selectedDish.comments : null)}
                </div>
            </div>
        )

    }


}
export default DishDetails;